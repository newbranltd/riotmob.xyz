'use strict';
/**
 * sass standalone task
 */
const gulp       = require('gulp');
const sass       = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const postcss    = require('gulp-postcss');

const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');

const join       = require('path').join;
const config     = require('../nb.config.js');

let processors = [
    autoprefixer({browsers: ['last 1 version']})
];

exports.devTask = function()
{
    return gulp.src(
        join(config.yeoman.app , 'styles' , 'main.scss')
    ).pipe(
        sourcemaps.init()
    ).pipe(
        sass().on('error' , sass.logError)
    ).pipe(
        postcss(processors)
    ).pipe(
        sourcemaps.write('./maps')
    ).pipe(
        gulp.dest(config.yeoman.app , 'styles')
    );
}; //.displayName = 'sass dev task'; // <-- new in V.4 (don't know if it works like this though)

exports.buildTask = function()
{
    const opts = {outputStyle: 'compressed'};
    processors.push(cssnano());
    return gulp.src(
        join(config.yeoman.app , 'styles' , 'main.scss')
    ).pipe(
        sass(opts).on('error' , sass.logError)
    ).pipe(
        postcss(processors)
    ).pipe(
        gulp.dest(config.yeoman.dest , 'styles')
    )
}; //.displayName = 'sass build task';
