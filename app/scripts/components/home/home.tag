<home>
    <div class="cf code">
        <div class="fl w-100 pa2 bg-light-gray">
            <section class="mw5 mw7-ns center pa3 ph5-ns">
                <h1 class="mt0">Riot.js V.2 tutorial</h1>
                <p class="lh-copy measure">
                    This was previously published on newbranch.cn site. The Riot V.3 tutorial is currently being written.
                    And this website is build with one of the <a href="https://github.com/joelchu/generator-rtjs" target="_blank">tool</a>
                    which will be feature in the next series of the tutorial.
                </p>
            </section>
        </div>
    </div>
    <section class="mw7 center">
          <h2 class="athelas ph3 ph0-l">Content</h2>

          <loader hide="articles.length"></loader>

          <article class="pv4 bt bb b--black-10 ph3 ph0-l" each={art , i in articles}>
              <div class="flex flex-column flex-row-ns">
                  <div class="w-100 w-60-ns pr3-ns order-2 order-1-ns">
                      <h1 class="f3 athelas mt0 lh-title">
                          <a href="{baseUrl}{art.slug}">{art.title}</a><!-- ?next={art.next}&amp;last={art.last} -->
                      </h1>
                      <p class="f5 f4-l lh-copy athelas">
                         &nbsp;
                      </p>
                  </div>
                  <div class="pl3-ns order-1 order-2-ns mb4 mb0-ns w-100 w-40-ns">
                      <img src={images[i]} class="db" alt="*" />
                  </div>
              </div>
              <time class="f6 db gray">Created: {formatDate(art.created_at)}</time>
              <time class="f6 db gray">Last Update: {formatDate(art.updated_at)}</time>
          </article>
    </section>

    <script>
        this.baseUrl = '#!/post/';

        // @TODO figure out the server side compiler
        // import {baseUrl} from 'services/config.json';

        import moment from 'moment';
        this.formatDate = (date) =>
        {
            return moment(date).format('D MMM YYYY');
        };

        this.on('update' , () =>
        {
            document.title = 'Riot.js V.2 tutorial @ riotmob.xzy (Riot.js V.3 coming soon) by Joel Chu 2016';
            if (this.opts.contents) {
                this.articles = this.opts.contents.articles.map( (art , i) =>
                {
                    art.last = i - 1;
                    art.next = i + 1;
                    return art;
                });

                this.images = this.opts.contents.images;
                // console.log(this.opts.contents);
            }
        });

    </script>
</home>
