
<app-route>
    <virtual data-is={component} contents={contents}></virtual>
    <script>
        import route from 'riot-route';

        import 'components/home/home.tag';
        import 'components/about/about.tag';
        import 'components/post/post.tag';
        // we only want to call the data call once so we promote it up here
        import Promise from 'promise-polyfill';
        import DataSrv from 'services/data-srv.js';
        // setup the base path where the articles should point to
        //baseUrl + 'static/article/';
        const dataSrvInstance = new DataSrv();

        this.contents = null;

        this.fetchData = () =>
        {
            if (this.contents === null) {
                Promise.all([
                    dataSrvInstance.getList(),
                    dataSrvInstance.getImages()
                ]).then( (result) => {
                    this.contents = {
                        articles: result[0],
                        images: result[1]
                    };
                    this.update();
                });
            }
            else {
                this.update(); // this is a bit stupid! if I don't call update the tag won't update
            }
        };
        this.component = 'home';

        this.setHome = (is) =>
        {
            this.parent.tags['app-nav'].trigger('change' , is);
        };

        route.base('#!');

        route((collection , params) =>
        {
            this.setHome(false);
            switch (collection) {
                case 'about':
                    this.component = 'about';
                break;
                case 'post':
                    this.component = 'post';
                    // this.fetchData();
                break;
                default:
                    this.setHome(true);
                    this.component = 'home';
                    this.fetchData();
            }
            this.update();
        });

        route.start(true);

    </script>
</app-route>
