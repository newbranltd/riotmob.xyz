<app-nav>

    <article class="mw5 center bg-white br3 pa3 pa4-ns mv3 ba b--black-10 helvetica" show={atHome}>
        <div class="tc">
            <img src="assets/IMG_0307.jpg" class="br-100 h4 w4 dib ba b--black-05 pa2" title="My cutie Tomato">
            <h1 class="f3 mb2">Tomato</h1>
            <h2 class="f5 fw4 gray mt0">CCO (Chief Cat Officer)</h2>
        </div>
    </article>

    <script>
        this.atHome = false;

        this.on('change' , (value) =>
        {
            this.update({'atHome': value});
        });

    </script>

</app-nav>
