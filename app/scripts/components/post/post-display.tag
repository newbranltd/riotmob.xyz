<post-display>
	<div>
		<yield />
	</div>

	<script>
		this.on('update' , () =>
		{
			this.root.innerHTML = opts.content.html;
		});
	</script>
</post-display>
