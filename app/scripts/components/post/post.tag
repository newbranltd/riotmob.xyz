<post>

	<article class="pa3 pa5-ns helvetica">
        <header class="fl w-100 pa2 center">
			<h1 class="f-headline-l">
				{article.title}
			</h1>
			<section class="dt dt--fixed">
				<div class="dtc tc pv4">
					<div class="ph3">
				  		<a class="f6 grow no-underline br-pill ph3 pv2 mb2 dib white bg-black" href="/#!home">HOME</a>
				  	</div>
				</div>

				<div class="dtc tc pv4">

				</div>

				<div class="dtc tc pv4">
					<!-- time class="f6 db gray">Created: {formatDate(article.created_at)}</time -->
					<time class="f6 db gray">{formatDate(article.updated_at)}</time>
				</div>
			</section>
		</header>

		<div class="pa3 ph7-l mw9-l center">
			<post-display content={article}></post-display>
		</div>

		<div class="pa3 ph7-l mw9-l center">
			<div class="flex items-center justify-center pa4">

				<a if={lastLink} href="#0" class="f5 no-underline black bg-animate hover-bg-black hover-white inline-flex items-center pa3 ba border-box mr4">
					<svg class="w1" data-icon="chevronLeft" viewBox="0 0 32 32" style="fill:currentcolor">
						<title>chevronLeft icon</title>
						<path d="M20 1 L24 5 L14 16 L24 27 L20 31 L6 16 z"></path>
					</svg>
					<span class="pl1">Previous</span>
				</a>

				<a if={nextLink} href="#0" class="f5 no-underline black bg-animate hover-bg-black hover-white inline-flex items-center pa3 ba border-box">
					<span class="pr1">Next</span>
					<svg class="w1" data-icon="chevronRight" viewBox="0 0 32 32" style="fill:currentcolor">
						<title>chevronRight icon</title>
						<path d="M12 1 L26 16 L12 31 L8 27 L18 16 L8 5 z"></path>
					</svg>
				</a>
			</div>
		</div>

	</article>

    <script>
		import moment from 'moment';
		import route from 'riot-route';
		import DataSrv from 'services/data-srv.js';
		import 'components/post/post-display.tag';

        const dataSrvInstance = new DataSrv();

		this.formatDate = (date) =>
        {
            return moment(date).format('D MMM YYYY');
        };
		this.article = {};
		const me = this;
		let fetched = false;

		/*
		this.on('updated' , () =>
		{
			if (!fetched) {
				fetched = true;
				if (this.opts && this.opts.contents) {
					const query = route.query();
					const contents = this.opts.contents;
					const last = parseInt(query.last);
					const next = parseInt(query.next);

					this.lastLink = contents[ last ];
					this.nextLink = contents[ next ];

					console.log(this.lastLink , last );
					console.log(this.nextLink , next );
				}
			}
		});
		*/

		// sub route is useless here because already capture in the app-route
		// const subRoute = route.create();
		// instead just examine the route in place
		route(function(comp , slug)
		{
			if (slug) {
				dataSrvInstance.getArticle(slug).then( (article) =>
				{
					document.title = article.title;
					me.article = article;
					me.update();
				}).catch( (error) =>
				{
					me.error = error;
					me.update();
				});
			}
		});
		route.exec();

	</script>

</post>
