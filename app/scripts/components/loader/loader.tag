<loader>

	<div class="w-100 center">
		<div class="loader">
			<div class={loaderClass}>
				<div></div>
				<div></div>
				<div></div>
				<div></div>
				<div></div>
			</div>
		</div>
	</div>

    <script>
		this.loaderClass = this.opts.loaderClass || "pacman";
	</script>

</loader>
