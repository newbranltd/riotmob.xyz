/**
 * dataSrv service
 * app/scripts/services/data-srv.js
 */
// import _ from 'lodash';
import fetch from 'fetch';


export default class dataSrv {

	/**
	 * class dataSrv constructor
	 */
	constructor()
	{

	}

	ok(res)
	{
		if (res.ok) {
			return res.json();
		}
		throw res;
	};

	getImages()
	{
		return fetch('/ask/images' , {method: 'get'}).then( this.ok );
	}

	getList()
	{
		return fetch('/ask/list' , {method: 'get'}).then( this.ok );
	}

	getArticle(slug)
	{
		return fetch('/ask/article/' + slug , {method: 'get'}).then( this.ok );
	}
}
