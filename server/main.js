'use strict';

const when = require('when');
const router = require('koa-router')();
const moment = require('moment');
const glob = require('glob');
const path = require('path');
const fs = require('fs');

// just grab the list
router.get('/list' , async (ctx , next) =>
{
    await next();
    const list = await ctx.db.list();
    ctx.body = list;
});
// fetching list of images
router.get('/images' , async (ctx , next) =>
{
    await next();
    const dest = process.env.NODE_ENV === 'production' ? 'dest' : '.dev';
    // https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
    const shuffle = function(array)
    {
        let currentIndex = array.length,
            temporaryValue = null,
            randomIndex = undefined;
        // While there remain elements to shuffle...
        while (0 !== currentIndex) {
            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;
            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }
        return array;
    };
    const base = path.join(__dirname , '..' , dest );
    const images = await when.promise( function(resolver , rejecter)
    {
        glob( path.join(base , 'assets' , 'cats' , '*.jpg') , function(err , list)
        {
            if (err) {
                return rejecter(err);
            }
            resolver(
                shuffle(list).map( (image) =>
                {
                    return image.replace( base , '');
                })
            );
        });
    });
    ctx.body = images;
})

router.get('/article/:slug' , async (ctx , next) =>
{
    const urls = ctx.request.url.split('/');

    if (urls[3]) {
        const article = await ctx.db.findBy('slug' , urls[3]);
        //const js = riot.compile(tag , {article: article});
        ctx.body = article;
    }
});

router.get('/gen' , async (ctx , next) =>
{
    await next();
    const list = await ctx.db.list();
    const fileOne = path.join(__dirname , '..' , 'app' , 'sitemap.xml');
    const fileTwo = path.join(__dirname , 'system' , 'newbranch.nginx.conf');

    let contentOne = '';
    let contentTwo = '';

    //console.log(list);

    list.forEach( (l) =>
    {
        let slug = l.slug;
        let link = "https://riotmob.xyz/#!/post/" + slug;
        let date = moment(l.updated_at).format('YYYY-MM-DD');
        contentOne += `
        <url>
            <loc>${link}</loc>
            <lastmod>${date}</lastmod>
        </url>
        `;
        contentTwo += `
            location /${slug} {
                return 301 ${link};
            }
        `
        /* didn't work 2017-07-08
        contentTwo += `
            location /${slug} {
                rewrite ^.* ${link} permanent;
            }
        `; */
    });

    contentOne += `</urlset>`;
    /*
    fs.writeFile(fileOne , contentOne , {flag: 'a'} , (err) =>
    {
        if (err) {
            throw err;
        }
    }); */
    fs.writeFile(fileTwo , contentTwo , {flag: 'a'} , (err) =>
    {
        if (err) {
            throw err;
        }
    });

    ctx.body = {ok: true};
});

module.exports = router;

// -- EOF --
