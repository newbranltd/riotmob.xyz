'use strict';
/**
 * init the database and export the database instance for the other operation
 *
 */
const sqlite3 = require('sqlite3').verbose();
const when    = require('when');
const join    = require('path').join;

// use a proper file instead
const dbType   = join(__dirname , 'newbranch.db'); //':memory:';

// create database connection
exports.connect = () =>
{
    return new sqlite3.Database( dbType  , (err) =>
    {
        if (err) {
            console.log('Fail to open db' , err);
            throw err;
        }
    });
};

// connect right here?
exports.db = (function() {
    if (!global.dbInst) {
        // console.log('init the GLOBAL.dbInst');
        global.dbInst = exports.connect();
    }
    return global.dbInst;
})();

// don't use this one write specific call
const run = function(db , sql , data = [])
{
    return when.promise( function(resolver , rejecter)
    {
        db.serialize( () =>
        {
            db.all(sql , data , function(err)
            {
                if (err) {
                    return rejecter(err);
                }
                resolver(true);
            })
        });
    });
};

exports.run = run;

// just select rowid , * from
exports.list = function()
{
    const db = exports.db;
    return when.promise( function( resolver , rejecter)
    {
        db.serialize( () =>
        {
            const sql = `SELECT * FROM posts WHERE status = ?`;
            db.all( sql , ['published'] , function(err , rows)
            {
                if (err) {
                    return rejecter(err);
                }
                resolver(rows);
            });
        });
    });
};

exports.findBy = function(key , value)
{
    const _db = exports.db;

    return when.promise( function(resolver , rejecter)
    {
        _db.serialize( () =>
        {
            const sql = `SELECT rowid , * FROM posts WHERE ${key} = ?`;

            _db.all(sql , [value] , function(err , rows)
            {
                if (err) {
                    return rejecter(err);
                }
                let result = {};
                if (rows && rows.length) {
                    result = rows[0];
                }
                resolver(result);
            });
        });
    });
};
