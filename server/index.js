'use strict';
/**
 * The main interface for the server to run
 * We are going to do this in a MVC style with Koa
 */
const path = require('path');
// koa
const koa = require('koa');
const serve = require('koa-static');
const router = require('koa-router')();

const koaBody = require('koa-body');
// const io = require('socket.io');

const sqlite = require('./sqlite.js');
// init the koa instance
const koaApp = new koa();

// add body parser middleware
koaApp.use(koaBody());

// for getting the real client ip address
koaApp.proxy = true;

    ////////////////////////
    //     STATIC FILES   //
    ////////////////////////

const root = path.resolve(
    path.join(__dirname , '..' , 'dest')
); // why the dest path is at this level?
// console.log('webroot' , root);
// console.log(process.env.NODE_ENV);
koaApp.use(serve(root));

// from http://spathon.com/koa-js-and-socket-io/

    ////////////////////////
    //      ROUTER        //
    ////////////////////////
// passing the db object to the rest of the middleware(s)
koaApp.use(async function(ctx , next)
{
    ctx.db = sqlite;
    await next();
});

// here include another collection of controller calls
const askRouter = require('./main.js');
askRouter.use('/ask' , askRouter.routes());
koaApp.use(
    askRouter.routes()
).use(
    askRouter.allowedMethods()
);
// another collection of controller calls
const staticRouter = require('./static.js');
staticRouter.use('/static' , staticRouter.routes());
koaApp.use(
    staticRouter.routes()
).use(
    staticRouter.allowedMethods()
);

    ////////////////////////
    //      RUN           //
    ////////////////////////

const port = 7890;
// change from koaApp.listen
koaApp.listen( process.env.PORT || port);

console.log(`Koa server start @ ${port}`);


// -- EOF --
