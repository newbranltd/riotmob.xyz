'use strict';
/**
 * static route
 */
const router = require('koa-router')();

const when = require('when');
const riot = require('riot');
const glob = require('glob');
const path = require('path');

const tag = require('./files/post-display.tag');

router.get('/nginx' , async (ctx , next) =>
{
    await next();
    const list = await ctx.db.list();
    const writeToConfig = (list) =>
    {


    };
    ctx.body = {ok: true};
});

router.get('/article/:slug' , async (ctx , next) =>
{
    const urls = ctx.request.url.split('/');

    if (urls[3]) {
        const article = await ctx.db.findBy('slug' , urls[3]);
        ctx.body = riot.compile(tag , {article: article});
    }
});

module.exports = router;
